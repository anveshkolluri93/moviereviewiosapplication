# README #

### What is this repository for? ###

* The primary reason to build this movie review app is it stands prevalent in helping the users assess the movie taking in to account the outstanding websites. There is no platform or application which makes decisions based on multiple websites. We consider the fact of the prominent websites attract several users in a wide range of spectrum. We assume this is an important one in making a decision. We try to provide a combined view by using the data of these pre-dominant websites and making a conclusions. It serves as a base for every user so that they can rely on our views on the movie as our sources are trusted and they can avoid the overhead of looking in to multiple websites and make a decision effectively.
* The homepage has many features to choose from. The prominently prevailing movies are advertised in the posters area. The user can choose the movie directly by clicking on it. We also plan to show the user’s history and his interests by keeping track of his searches. Each user is given a separate account to make use of the application. 
The application also ensures to show the detail of every movie with the cast and crew. The movies can be sorted based on the views of the users as positive ones on the top and the lesser ones following them. They are also listed based on the date of release, where the very recent ones are listed on the top. 
Now the user can read the movie description with the ratings listed below. These ratings are derived from the prominent websites who dominate the film industry’s marketing by getting views from a huge crowd of people from various backgrounds. 

* Swift 2.0

### How do I get set up? ###

* Using Xcode 7.3 as IDE
* Kinvey Framework to handle the User Database
* Testing is done on Ipad2.

### Instructions ###
*	Run the application in the simulator with Ipad2 configuration.
*	In order to view the story board select Regular Any format
*	First is the Launch screen with advertisements
*	Click on the launch app button to navigate into the application
*	The page sneak peek appears with very little detail.
*	On clicking on any one of the cell it redirects for login.
*	By clicking on register button you can register in the application with valid details.
*	Once registered the user is directed to login page, where the user can login to the application with the registered credentials.
*	The user can then view the table view of all the movies with brief information, nothing is masked.
*	The user can select any movie to get additional details page with ratings from two different websites Rotten Tomatoes and iMDB
*	The user can also give rating using star rating above Give Your Rating label
*	The user can logout by clicking on the logout button at the top right.
*	Any time user can click on the profile page to look at the profile of the user logged in 
*	The user can also look at the history he/she browsed by clicking on the history button

### Contribution guidelines ###

* Writing tests
* Code review

### Wanna talk to me about it? ###

* Anveshkumar Kolluri
* anveshkolluri93@gmail.com